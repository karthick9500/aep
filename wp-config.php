<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root123');

/** MySQL hostname */
define('DB_HOST', '54.169.41.17');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


$currenthost = "http://".$_SERVER['HTTP_HOST'];
$currentpath = preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME']));
$currentpath = preg_replace('/\/wp.+/','',$currentpath);
define('WP_HOME',$currenthost.$currentpath);
define('WP_SITEURL',$currenthost.$currentpath);
define('WP_CONTENT_URL', $currenthost.$currentpath.'/wp-content');
define('WP_PLUGIN_URL', $currenthost.$currentpath.'/wp-content/plugins');
define('DOMAIN_CURRENT_SITE', $currenthost.$currentpath );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't&ZA~/DiDT;]xF=Nu2p+OdFgLz>MzcKya]]gE`$cC9zA`R!zSDM[)VTUq%bsK.;}');
define('SECURE_AUTH_KEY',  '?4{+78ap75Q&K?qy>x5=SD$CVnnq3u]&Q4)r@V5dPK)SB[:wnaW7{G#7y)gd/)/2');
define('LOGGED_IN_KEY',    'yFy-cs0N|Dw2Wb-iKs*}]FXB2x#?+Y3yB(o:9,0-`E8a[|_2;6Es-<DQ/=q/9b}#');
define('NONCE_KEY',        'L/=_bIi{{p&)$s[(h5s{|@3{:i~ZopHwP@/(;mszvLqM/1dz;&5}1V2u2Jr^ICn9');
define('AUTH_SALT',        '#~R3{B(wAB^P&hN.+NI&rldkUd!jg6:XaS0U3R;J4HGq6w:z;1pv`$>$dI4KBFy:');
define('SECURE_AUTH_SALT', '-Z:Ng~k>%?mL-21Jh#Zy2](KY-S`k44Uz=`IVP`+R1,}o0L3,5*n?L N<w5m-v]K');
define('LOGGED_IN_SALT',   'fDHC$zVKXj NSpRY|O24R<aT(;{rS?tcn..hO>Jb0rBk_oOP&~QF>;R)f6`$nee3');
define('NONCE_SALT',       'dKWevdMgM1/ C0Qn5N v _|8o_Q1dwy1.qJpEh}FK+L=2`9br1k!9YjK:3,]2k_g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

